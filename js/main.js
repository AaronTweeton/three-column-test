requirejs.config({
    paths: {
        jquery: 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min'
    }
});

requirejs(['debug']);

define(['jquery'], function ($) {

    $(function () {
        /* Function to equalize heights of divs, since flexbox isn't supported in IE9 */
        var equalizeHeights = function () {
            /* Only equalize heights for desktop screens. */
            if (window.innerWidth > 1024) {
                /* Reset height upon resize. */
                $('.block-inner').css('height', 'auto');

                var maxHeight = 0;
                $('.block-inner').each(function () {
                    if ($(this).height() > maxHeight) {
                        maxHeight = $(this).height();
                    }
                });
                $('.block-inner').height(maxHeight);
            } else {
                $('.block-inner').css('height', 'auto');
            }
        }
        /* Initialize height equalizing. */
        equalizeHeights();

        /* Equalize height again when browser resized. */
        window.onresize = function () {
            equalizeHeights();
        }


        /* Accordion function */
        var accordion = function () {
            // Append link for click handling after each title
            $('.block-title-text').after('<a href="#" class="collapse collapse-closed">+</a>')

            // Add click handler for link
            $('.collapse').on('click', function (e) {
                /* Cache $this */
                var $this = $(this);

                /* Show content when handler clicked. */
                $this.closest('.trifecta').find('.block-content').toggleClass('content-open');

                /* Change display state of handler. */
                $this.toggleClass('active');
            });
        }

        accordion();
    });
})